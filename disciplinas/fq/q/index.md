---
layout: page
title: Química
permalink: /fq/q/
---

# Química 10º Ano

Pela ordem que a professora deu a matéria e não pela ordem do livro:

[Medições em Química]({% link disciplinas/fq/q/1.MedicoesEmQuimica.md %})  
[Apontamentos para o 1º Teste]({% link disciplinas/fq/q/ApontamentosParaO1Teste.md %})  

## 1.2. Energia dos eletrões nos átomos

[1.2.1 Espetros contínuos e descontínuos]({% link disciplinas/fq/q/1.2.1.md %})  
