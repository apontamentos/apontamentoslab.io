---
layout: page
title: Apontamentos para o 1º teste de Química
permalink: /fq/q/1teste/
---

### Matéria para o teste:

Páginas: 10*, 11*, 12, 13, 23, 24, 25, 26*, 201, 202, 203

\* páginas incompletas

##
## Ordens de grandeza e escalas de comprimento

* Toda a matéria é constituída por átomos;
* O átomo deriva do grego _a+thomos_ e significa sem divisão ou divisão mais pequena de matéria.
* O átomo é constituído por partículas ainda mais pequenas sendo por isso uma partícula divisível.
* O átomo é constituído pelo **núcleo**, que é constituído por **protões** (+) e **neutrões** (sem carga elétrica), e pelos **eletrões** (-) que se movem à volta do **núcleo**.

<center>Cargas elétricas dos constituintes do átomo:</center>

<center>
| Constituinte | Carga            |
|--------------|------------------|
| Protão       | Positiva         |
| Neutrão      | Neutra/Sem carga |
| Eletrão      | Negativa         |
| Átomo        | Neutra/Sem carga |
</center>

* É no núcleo que se encontra quase toda a massa do átomo, já que a massa dos eletrões é muito inferior quando comparada com a dos protões e neutrões.
* A massa do protão é aproximadamente igual à do neutrão ($ 1,672 \times 10^{-27} kg \approx 1,675 \times 10^{-27} kg$).
* Os átomos **não têm carga elétrica**, já que **o número de protões é sempre igual ao número de eletrões**. Os átomos são, por isso, eletricamente neutros.
* Para caracterizar um átomo é utilizado o **número atómico** e o **número de massa**:
	* **Número atómico (Z)** - corresponde ao número de **protões** (que é igual ao número de eletrões). Caracteriza um elemento químico, todos os átomos de um certo elemento químico têm o mesmo número de protões. 
	* **Número de massa (A)** - corresponde à **soma do número de protões e eletrões** (número de partículas no núcleo). Átomos do mesmo elemento químico podem ter diferentes números de massa (já que o número de neutrões pode variar).
	 
<center>Para calcular o número de neutrões:</center>

$$ A - Z $$
* Todos os átomos que tiverem o mesmo número de protões têm o mesmo elemento químico.
* Dá-se o nome de **isótopos** aos átomos do mesmo elemento (mesmo número de protões) mas com diferente número de neutrões. Por isto, os isótopos têm o mesmo número atómico (Z) mas diferente número de massa (A).
	* Quando se escreve carbono-13, está-se a referir ao isótopo de carbono com número de massa 13.
	* Isótopo vem do grego _iso+thopos_ que significa "o mesmo lugar" (o mesmo lugar na tabela periódica).

<center>Para representar um átomo é utilizada a seguinte notação:</center>

$$^A_Z X^q$$

Sendo que:
* X representa o símbolo químico;
* A representa o número de massa;
* Z representa o número atómico;
* q representa a carga elétrica.

##
## Quantidade de matéria e massa molar

* A **quantidade de matéria** é simbolizada pelo símbolo _n_. 
* A unidade da quantidade de matéria é a **mole** (símbolo mol) cujo o valor é o **número de Avogadro** que é $6,02\times10^{23}$.
	* (Representa o número de átomos existentes em 0,012kg de carbono-12).


_Exemplo:_

<center>1 mol de moléculas de água ($H_2O$) tem $6,02\times10^{23}$ moléculas de água.</center>

* A **constante de Avogadro** ($6,02\times10^{23}$) (em química) representa o número de entidades existentes numa mole de qualquer substância. 
	* o seu símbolo é $N_A$ ou $L$.
* O **número de entidades** (N) é diretamente proporcional à quantidade de matéria (n), sendo a sua constante o número de Avogadro (N$_A$):
$$ N = n \times N_A $$

_Exemplo:_

Calcula o número de moléculas em 5 mol de $H_2$:

$$N=n\times N_A$$

$$N=5 \times 6,02 \times 10^{23}$$

$$N=3,01 \times 10^{24}$$

<center>**ou**</center>

$$\frac{1}{6,02 \times 10^{23}}=\frac{5}{x}$$

$$x=\frac{5 \times 6,02 \times 10^{23}}{1}=3,01 \times 10^{24}$$

* A **massa molar** (M) indica a massa por unidade de quantidade de matéria (ou seja, por cada mole) de uma certa substância e a sua unidade é g/mol.
	* A **massa molar** é numericamente **igual** à **massa relativa** ($M = M_r$).

		* _Exemplo:_

			* $M_r (H_2O) = 2 \times 1,00 + 1 \times 16,00 = 18,00$

		* _Por isso:_

			* $M (H_2O)= 18,00 g / mol$

_Exercícios Exemplo:_

<center>
Calcula a massa de 2 mol de $H_2O$
</center>

$$M=\frac{m}{n}$$
$$M=18,00 g/mol; n=2 mol; m=x$$
$$18=\frac{x}{2} \Leftrightarrow x=36g$$

##
## Fração molar

A fração molar de uma substância numa mistura é obtida pelo quociente da quantidade dessa substância pela quantidade total de substâncias na mistura:

<center>Fração molar, $x(A)$</center>

$$x(A)=\frac{n_A}{n_T}$$

<center>$n_A$ - quantidade de matéria de A</center>
<center>$n_T$ - quantidade total de matéria</center>

A fração molar não tem unidadade e a soma de todas a frações molares de uma substância é igual a 1.

##
## Medição

* Ao resultado de uma **medição** dá-se o nome de **medida** ou **valor medido** (Medição - processo; Medida - resultado da medição).
* As medições podem ser **diretas** ou **indiretas**:
	* São **diretas** quando a medida é obtida **através da leitura de um aparelho de medição;**
	* São **indiretas** quanda a medida é obtida **através de cálculos que por si utilizam valores de medições diretas.**
* As medições diretas são realizadas com aparelhos de medição que podem ser **analógicos** (têm uma escala _(Ex: proveta)_ ou uma marca de referência _(Ex: balão volumétrico)_ ) ou **digitais** (têm um mostrador digital _(Ex: termómetro)_ . 
* Todas as medições, independentemente da qualidade do aparelho de medição ou da experiência do operador, vêm acompanhadas de uma **incerteza**, porque qualquer medição está sujeita **erros de medição** (ou erros experimentais).
	* Existem dois tipos de erros de medição: **erros sistemáticos** e **erros aleatórios**.
		* Os **erros sistemáticos**, como o nome indica, são erros que se repetem consistentemente - todas as medições são influenciadas no mesmo sentido - e podem ser corrigidos se a causa do problema for eliminada.
			* Exemplo: Aparelhos de medição mal calibrados.
		* Os **erros aleatórios**, como o nome indica, são aleatórios e **imprevisíveis** e não podem ser eliminados na totalidade.
			* Exemplo: Erros humanos, variação da temperatura do material, etc.
##
* A **incerteza** representa a variação das medidas obtidas.
	* Quando só é realizada uma medição, à incerteza do aparelho dá-se o nome de **incerteza absoluta de leitura**.
	* Para determinar a incerteza absoluta de leitura, os critérios variam com o tipo de instrumentos de medição:
		* O aparelho diz qual é a incerteza (normalmente antecedida por $\pm$);
		* O aparelho é analógico:
			* Metade da menor divisão de escala.
		* O aparelho é digital:
			* Menor valor lido no ecrã.
##

* Diferença entre **exatidão** e **precisão**
	* **Exatidão** - "distância" entre os valores medidos e o valor verdadeiro. Está relacionada com os **erros sistemáticos**: + erros sistemáticos, - exatidão;
	* **Precisão** - "proximidade" entre os valores medidos. Está relacionada com o **erros aleatórios**: + erros aleatórios, - precisão.

