# Gramática - Português 10º Ano

## Processos fonológicos

### Inserção (PEP engorda)

**Prótese** - acrescentam-se "letras" no princípio da palavra.  
**Epêntese** - acrescentam-se "letras" no meio da palavra.  
**Paragoge** - acrescentam-se "letras" no fim da palavra.  

### Supressão (ASA corta)

**Aférese** - retiram-se "letras" no princípio da palavra.  
**Síncope** - retiram-se "letras" no meio da palavra.  
**Apócope** - retiram-se "letras" no fim da palavra.  

