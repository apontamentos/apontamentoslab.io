---
layout: page
title: Gramática
permalink: /por/g/
---

## Fonética e fonologia

* [Processos fonológicos]({% link disciplinas/por/g/ProcessosFonologicos.md %})

## Sintaxe

* Coordenação e subordinação
  - [Coordenação]({% link disciplinas/por/g/Coordenacao.md %})
  - [Subordinação]({% link disciplinas/por/g/Subordinacao.md %})
