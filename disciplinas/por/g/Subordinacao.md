---
layout: page
title: Subordinação
permalink: /por/g/Subordinacao/
---

* **Frases complexas** - frases com dois ou mais grupos verbais.
  - Podem ser formadas por **coordenação** ou por **subordinação**.

## Tipos de orações subordinadas

### Adverbiais

| Orações Subordinadas | Conjunções e locuções                        |
| -------------------- | -------------------------------------------- |
| Causais              | porque, visto que, já que                    |
| Consecutivas         | que, de modo que                             |
| Condicionais         | se, caso, desde que,                         |
| Concessivas          | no entanto, embora, apesar de que, ainda que |
| Comparativas         | como                                         |
| Temporais            | antes que, logo que, assim que               |
| Finais               | para que                                     |

_**Exemplos:**_

### Adjetivas

| Orações Subordinadas   | Conjunções e locuções |
| ---------------------- | --------------------- |
| Relativas restritivas  | que, quem,            |
| Relativas explicativas | cujo, onde            |

_**Exemplos:**_

### Substantivas

| Orações subordinadas                 | Conjunções e locuções |
| ------------------------------------ | --------------------- |
| Relativas sem antecedente            | (o) que, onde, quem   |
| Completivas                          | que                   |
| Completivas interrogativas indiretas | que, se, para         |
| Infinitivas                          | para                  |

_**Exemplos:**_
