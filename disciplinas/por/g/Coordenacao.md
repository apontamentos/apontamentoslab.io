---
layout: page
title: Coordenação
permalink: /por/g/Coordenacao/
---

* **Frases complexas** - frases com dois ou mais grupos verbais.
  - Podem ser formadas por **coordenação** ou por **subordinação**.

## Tipos de orações coordenadas

| Orações Coordenadas | Conjunções e locuções        |
|:-------------------:|:----------------------------:|
| Copulativas         | e, nem... nem, também        |
| Adversativas        | mas, porém, todavia          |
| Disjuntivas         | ou, ora... ora, quer... quer |
| Explicativas        | porquanto, pois, que         |
| Conclusivas         | logo, portanto, pois         |
