---
layout: page
title: Contextualização da Poesia Trovadoresca
permalink: /por/el/c/
---

* Surgiram durante a Idade Média;

* O seu espaço social era a nobreza:
  - Nas cortes senhoriais e na corte régia, que competiam com cortes estrangeiras, "lutando" por ser a corte mais culta;
  - Os trovadores eram geralmente bastardos, mas acolhidos pela corte devido ao seu talento, e cavaleiros sem fortuna, membros da corte que esta sustentava e lhes confiava o entretenimento nas horas de lazer.
  - Os temas das cantigas eram o amor cortês, o prestígio social e histórias de fidelilidade e traição, que deram origem às cantigas de amigo e de amor e às cantigas de escárnio e maldizer.

* Eram cantadas e escritas em **galego-português**, uma língua falada na região ocidental da Península Ibérica (atual Portugal + Galiza) derivada do latim sendo utilizada desde o século X/XII até ao ao século XIV. No que toca à poesia galaico-portuguesa, a língua não era utilizada exclusivamente pelos trovadores das áreas do atual Portugal e Galiza como o seu nome pode dar a entender, mas era utilizada por variadíssimos autores de toda a Península Ibérica.
 
* Constituem a lírica galaico-portuguesa 1680 textos de assunto profano, guardados em 3 cancioneiros manuscritos (Cancioneiro da Ajuda (maioritariamente cantigas de amor), Cancioneiro da Biblioteca Nacional e o Cancioneiro da Vaticana) e 420 textos de assunto religioso chamadas _Cantigas de Santa Maria_. Todos eles eram escritos no anteriormente falado galego-português e foram escritos entre o século XII e a segunda metade do século XIV. Ainda que alguns dos autores destes textos sejam anónimos, é possível saber que as cantigas eram escritas por elementos da nobreza (rei, filhos do rei, elementos da corte), po elementos do clérigo ou até mesmo membros do povo que competiam com a nobreza conseguindo muitas vezes igualar a sua habilidade.
 
* As obras eram em verso porque antes, antes de serem escritas, as obras eram faladas/cantadas e o verso era uma excelente de maneira de ritmar a fala, facilitando a memorização. 

* Haviam 2 grandes tipos de poesia trovadoresca: a lírica-amorosa, expressa através das cantigas de amigo e das cantigas de amor, e a satírica, expressa através das cantigas de escárnio e maldizer. Chamam-se cantigas porque estavam fortemente ligadas à música, sendo cantadas juntamente com instrumentos, ainda que se tenham perdido a maioria das pautas musicais impedindo-nos de conhecer a verdadeira essência da poesia trovadoresca.
