---
layout: page
title: Cantigas de amigo
permalink: /por/el/pt/ca/
---

* Cantigas no qual o emissor é uma **donzela** que exprime os seus sentimentos às **amigas**, à **mãe** ou à **natureza** sobre a ausência do seu **amigo**/amado.

(Como está na ficha formativa nº 3):

* Uma das principais características das cantigas de amigo é o **amor não correspondido**, devido a ausência ou indiferença do amigo.
* Além disso, encontram-se "testemunhas" desse amor que tanto podem ser a natureza (flores, árvores, pássaros, etc) ou pessoas (amigas, mãe).
* E encontra-se uma intimidade com a natureza, como uma "afinidade mágica".

### Caracterização formal

Na lírica galego-portuguesa são utilizados três tipos de **Paralelismo**:
* Literal/de palavra;
* Estrutural/de construção (sintática ou rítmica).
* Concetual/de pensamento.

Basicamente, o paralelismo consiste na repetição de palavras, versos inteiros ou conceitos ao longo do poema, que se pode tornar entediante e, para combater isso, os trovadores recorriam à **variação**:
* Substituição de palavra rimante por um sinónimo (o mais utilizado);
* Transposição de palavras;
* Repetição do conceito mediante da negação do conceito oposto.

Grande parte das cantigas que apresenta paralelismo apresenta coblas constituídas por dísticos monórrimos seguidos de um rofrão monóstico. Nestas cantigas encontra-se o processo de encadeamento _**leixa-prem**_ .

_**Leixa-prem**_ :
* O 1º dístico é igual ao 2º dístico, variando apenas a palavra rimante mas sem mudança de sentido;
* O 1º verso do 3º dístico é o 2º verso do 1º dístico (_leixa-prem_ propriamente dito) e surge um novo verso de seguida;
* O 2º verso do 4º dístico é o 2º verso do 2º dístico (_leixa-prem_) seguido de uma variação do 2º verso do 3º dístico;
* Se o processo se verificar no resto da cantiga e o número de coblas for par, encontra-se um **paralelismo perfeito**.


