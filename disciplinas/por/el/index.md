---
layout: page
title: Educação Literária
permalink: /por/el/
---

## Poesia Trovadoresca

* [Contextualização da poesia trovadoresca]({% link disciplinas/por/el/pt/Contextualizacao.md %})
* [Cantigas de Amigo]({% link disciplinas/por/el/pt/CantigasDeAmigo.md %})  
