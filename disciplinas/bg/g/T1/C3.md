---
layout: page
title: 3. A medida do tempo e a idade da Terra
permalink: /bg/g/t1/c3/
---

## 3.1 Idade relativa e idade radiométrica

* Um **fóssil** é um resto de um organismo ou vestígio da sua atividade que se encontra preservado nos estratos das rochas sedimentares.
* Os fósseis permitem-nos conhecer as características do meio em que foram formados;
* Os fósseis normalmente estão presentes nas rochas sedimentares;

### Datação relativa

* **Princípio da sobreposição** - numa sucessão de estratos não deformados, um estrato é mais antigo do que aquele que está por cima e mais recente do que aquele que está por baixo.

  - Consequentemente, se os estratos contiverem fósseis, os fósseis dos estratos superiores são mais recentes que os fósseis dos estratos inferiores.
  - No entanto, existem exceções a este princípio:
 
    * Se os estratos apresentarem dobras ou falhas, não é possível determinar com certeza que um estrato se encontra acima de outro;
    * Nos terraços/depósitos fluviais, os estratos mais recentes até se encontram em maior profundidade que os mais antigos;
    * Nos depósitos subterrâneos em grutas, as rochas que estão dentro das grutas são mais recentes que as rochas que rodeiam a gruta.

* **Princípio da Identidade Paleontológica** - estratos que tenham o mesmo conteúdo fossilífero são contemporâneos (ou estratos que tenham fósseis da mesma espécie formaram-se ao mesmo tempo). 

### Datação absoluta/radiométrica

* Encontram-se três tipos de átomos na natureza:
  - Átomos com igual número de protões e neutrões, são os mais abundantes;
  - Átomos com número de neutrões diferente do número de protões, mas estáveis;
  - Átomos com número de neutrões diferente do número de protões, instáveis.

* As formas instáveis dos elementos químicos designam-se por **isótopos radioativos** e são estes que são utilizados na datação absoluta.

* Estas formas instáveis apresentam uma propriedade - **decaimento radioativo** - que consiste na transformação de um átomo noutro com libertação de energia.
  - Cada átomo tem uma constante de decaimento, que é utilizada nos cálculos da datação absoluta;
  - O decaimento radioativo é irreversível, ou seja, depois de se transformar o isótopo não pode voltar a ter as características iniciais.

* Quando uma rocha se forma, apresenta sempre uma certa quantidade destes isótopos radioativos nos minerais que a constituem. Estes isótopos vão-se transformando ao longo do tempo, tornando-se em átomos estáveis.
  - Aos isótopos instáveis dá-se o nome de **átomos-pai**;
  - Aos átomos que resultam da transformação dá-se o nome de **átomos-filho**.
* Ao tempo necessário para que metade de uma certa quantidade de átomos-pai se transforme em átomos-filho dá-se o nome de **tempo de semivida**.

* A datação relativa é mais eficaz quando aplicada a **rochas magmáticas** já que quando ocorre a consolidação do magma, este transfere para os seus cristais uma certa quantidade de isótopos radioativos.

* No entanto, não se aplica às rochas sedimentares nem às metamórficas porque:
  - Durante o metamorfismo, podem se, por exemplo, perder átomos-filhos o que afetará os cálculos;
  - As rochas sedimentares são constituídas por uma variedade de rochas pré-existentes o que dificulta a datação.

## 3.2. Memória dos tempos geológicos


