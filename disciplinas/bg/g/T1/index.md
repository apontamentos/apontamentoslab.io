---
layout: page
title: Tema 1 - A geologia, os geólogos e os seus métodos
permalink: /bg/g/t1/
---

# Tema 1 - A geologia, os geólogos e os seus métodos

[Capítulo 1]({% link disciplinas/bg/g/T1/C1.md %}) - A Terra e os seus subsistemas em interação  
[Capítulo 2]({% link disciplinas/bg/g/T1/C2.md %}) - As rochas - arquivos que relatam a história da Terra  
[Capítulo 3]({% link disciplinas/bg/g/T1/C3.md %}) - A medida do tempo e a idade da Terra  
Capítulo 4 - A Terra - Um planeta em mudança  


