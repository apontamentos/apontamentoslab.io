---
layout: page
title: Apontamentos para o 1º teste de Inglês
permalink: /ing/1teste/
---

## Vocabulary
Sports & Music

## Grammar

### Present Simple

* Present form of the verb;
* 3rd Person Singular has an "s" at the end of the verb;
* Auxiliary verb is **"do"**, it's used in the **negative** and **interrogative** form of the verb;
* It's used in:
	* Timetables _(e.g.: The library **opens** at 10:00AM);_
	* Permanent states _(e.g.: I **live** in Portugal);_
	* Habitual actions/Routines _(e.g.: I **study** every day);_
	* Scientific and universal truths _(e.g.: Water **boils** at 100ºC)._

### Present Continuous

* Present form of the verb **"to be"** + (main verb + "-ing");
* Auxiliary verb is still the verb "to be" in the negative and in the interrogative forms;
* It's used in:
	* Actions which are in progress now _(e.g.: I **'m studying** for the English test);_
	* Repeated temporary actions _(e.g.: Every time I see her, she **'s complaining** about something);_
	* Fixed arrangements in the near future _(e.g.: I **'m visiting** Instituto Superior Técnico this weekend);_
	* Temporary situations for a short period of time _(e.g.: A cousin of mine **is studying** in Belgium)._
	
### Present Perfect

* Present form of the verb **"to have"** + **past participle** form of the main verb (usually main verb + "ed");
* It's used:
	* To describe a state which lasts up to the present _(e.g.: I **'ve been** living in Lisbon for 15 years);_
	* To describe recent events _(e.g.: I **have** just **finished** reading this book);_
	* To describe a habitual action in a period of time which continues up to the present _(e.g.: I **have** always **eaten** fruit after lunch);_
	* After "this is the first time..." _(e.g.: This is the first time I **have** ever **eaten** sushi)._
* Time expressions:
	* Already _(e.g.: I have already been to London)_ ;
	* Yet _(e.g.: I haven't been to London yet)_ ;
	* Just _(e.g.: I have just run 10Km)_ ;
	* For _(e.g.: I have been living in London for over 20 years)_ ;
	* Since _(e.g.: I have been living in London since 1997)_ .
