---
layout: page
title: Capítulo 2
permalink: /fil/1/2/1/
---

## 1. O que é um argumento?

* Um argumento é um conjunto de proposições em que uma delas é defendida pelas outras - as **premissas** apoiam a **conclusão**.
  - Podem haver várias premissas mas só uma conclusão.
  - O que é então uma proposição?
    * Uma proposição é uma frase declarativa que tem um valor de verdade (verdadeiro/falso), ainda que não se saiba o seu valor de verdade.
