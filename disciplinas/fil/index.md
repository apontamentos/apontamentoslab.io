---
layout: page
title: Filosofia
permalink: /fil/
---

## Unidade 1

### Capítulo 1

[1. Alguns problemas filosóficos e as disciplinas que os estudam]({% link disciplinas/fil/1.1.1..md %})  
[2. Que tipo de problemas estudam os filósofos? O que são problemas filosóficos?]({% link disciplinas/fil/1.1.2..md %})  
[3. O método da filsofia]({% link disciplinas/fil/1.1.3..md %})  
[4. A atitude filosófica]({% link disciplinas/fil/1.1.4..md %})  

### Capítulo 2

_Pequeno prefácio_

Como se acabou de estudar, os filósofos procuram transformar crenças fundamentais em problemas e encontrar as suas respostas.  
Para responder a estes problemas **não empíricos** utilizam a argumentação.

Como a argumentação tem um papel tão importante na atividade filosófica, é estudada previamente:

[1. O que é um argumento?]({% link disciplinas/fil/1.2.1..md %})  
[2. O que é clarificar argumentos? A análise e clarificação de argumentos]({% link disciplinas/fil/1.2.2..md %})  
