---
layout: page
title: Capítulo 1
permalink: /fil/1/1/1/
---

## 1. Alguns problemas filosóficos e as disciplinas que os estudam

* **Filosofia** - Amor pelo saber (_philos_ - amor, _sofia_ - saber).
* Algumas áreas da filosofia (a **negrito** as que são estudadas no 10º Ano):
  - **Metafísica**:
    * Disciplina que estuda os aspetos gerais da realidade;
    * _Exemplo_: O problema do livre arbítrio.
  - **Ética**:
    * Disciplina que procura responder ao problema de como devemos viver;
    * _Exemplo_: distinção entre "o que é certo e o que é errado".
  - **Política**:
    * Disciplina, muito ligada à ética, que reflete sobre as interações entre o Estado e os cidadãos;
    * _Exemplo_: O pagamento de impostos.
  - **Filosofia da Arte**:
    * Disciplina que discute, entre outras questões, o que torna algo uma obra de arte;
  - **Filosofia da Religião**:
    * Disciplina que analisa as crenças religiosas mais básicas, colocando-as em questão, verificando se são justificadas ou não;
    * _Exemplo_: a existência de Deus;
  - Filosofia do Conhecimento:
    * Disciplina que interroga e analisa a origem, a justificação e os limites das nossas crenças; 
    * _Exemplo_: O que é o conhecimento?
  - Lógica:
    * Disciplina que estuda a validade dos argumentos;
    * _Exemplo_: Que tipos de argumentos existem?
