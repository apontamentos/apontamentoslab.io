---
layout: page
title: Lógica e Teoria dos Conjuntos
permalink: /mat/1/
---

1. Proposições  
	[1. Valor lógico de uma proposição]({% link disciplinas/mat/LogicaETeoriaDosConjuntos/1.1..md %})  
	[2. Operações com proposições]({% link disciplinas/mat/LogicaETeoriaDosConjuntos/1.2..md %})  
	[3. Prioridades das operações lógicas]({% link disciplinas/mat/LogicaETeoriaDosConjuntos/1.3..md %})  
	[4. Relações lógicas entre as diferentes operações]({% link disciplinas/mat/LogicaETeoriaDosConjuntos/1.4..md %})  
	[5. Propriedades da conjunção e da disjunção]({% link disciplinas/mat/LogicaETeoriaDosConjuntos/1.5..md %})  
	[6. Primeiras Leis de De Morgan]({% link disciplinas/mat/LogicaETeoriaDosConjuntos/1.6..md %})  
	7. Resolução de problemas  
2. Condições e conjuntos  
	[1. Expressão proposicional ou condição]({% link disciplinas/mat/LogicaETeoriaDosConjuntos/2.1..md %})  
	[2. Quantificador universal e quantificador existencial. Segundas Leis de De Morgan. Contraexemplos.]({% link disciplinas/mat/LogicaETeoriaDosConjuntos/2.2..md %})  
	[3. Conjunto definido por uma condição. Igualdade de conjuntos. Conjuntos definidos em extensão.]({% link disciplinas/mat/LogicaETeoriaDosConjuntos/2.3..md %})  
	[4. Operações com conjuntos]({% link disciplinas/mat/LogicaETeoriaDosConjuntos/2.4..md %})  
	[5. Relações entre operações lógicas com condições e operações com os conjuntos que definem]({% link disciplinas/mat/LogicaETeoriaDosConjuntos/2.5..md %})  
	6. Negação de uma implicação universal. Demonstração por contrarrecíproco  
	7. Resolução de problemas  
