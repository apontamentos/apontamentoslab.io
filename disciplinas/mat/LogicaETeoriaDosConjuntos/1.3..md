---
layout: page
title: 1. Proposições
permalink: /mat/1/1/3/
---

## 1.3 Prioridades das operações lógicas

A não ser que sejam utilizados parênteses, normalmente são respeitadas as seguintes prioridades:

<center>
| Prioridades das operações (de cima para baixo)   |
| :----------------------------------------------: |
| _~_					      |
| $\land$ e $\lor$				 |
| $\Rightarrow$ e $\Leftrightarrow$		|
</center>
